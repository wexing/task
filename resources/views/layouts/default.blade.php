<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>@yield('title')</title>
    @include('partials.header')
    @yield('styles')
  </head>
  <body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand">@yield('greeting')</a>
            @yield('nav-links')
            @if(Auth::check())
                <a href = "{{route('logout')}}">Logout</a>
            @endif
        </nav>
        <div id="main">
            @yield('content')
        </div>
    
        <footer class="row">
            @yield('scripts')
            @include('partials.footer')
        </footer>
    </div>
  </body>
</html>