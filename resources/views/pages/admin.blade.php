@extends('layouts.default')

@section('content')
    <div id="app" class = "mt-3">
        <router-view></router-view>
    </div>
@endsection

@section('nav-links')
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="#/">Users</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#/requests">Requests</a>
        </li>
    </ul>
@endsection

@section('scripts')
    <script>
        var register_url = "{{route('register')}}";
        var feature_url = "{{route('features_update')}}";
        var request_url = "{{route('request_update')}}";
        var users = {!!$users!!};
        var requests = {!!$requests!!};
        var level = "admin";
    </script>
    <script src="{{ asset('js/vue/app.js') }}"></script>
@endsection

@section('title', 'Backend developer')
@section('greeting', "Hello, " . Auth::user()->fullname)