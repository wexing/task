@extends('layouts.default')

@section('content')
    <div id="app" class = "mt-3">
        <router-view></router-view>
    </div>
@endsection

@section('nav-links')
@endsection

@section('scripts')
    <script>
        var features = {!!$features!!};
        var offices = {!!$offices!!};
        var notifications = {!!$notifications!!};
        var level = "employee";
        var request_url = "{{route('request_create')}}";
        var notification_url = "{{route('notification_seen')}}";
        var requests = {!!$requests!!};
        var request_update_url = "{{route('request_update')}}";
    </script>
    <script src="{{ asset('js/vue/app.js') }}"></script>
@endsection

@section('title', 'Backend developer')

@section('greeting', "Hello, " . Auth::user()->fullname)