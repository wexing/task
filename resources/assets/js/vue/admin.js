/**
 * Administrator vue store
 */

import Users from './pages/admin/UsersComponent.vue';
import Requests from './pages/admin/RequestsComponent.vue';
import Vuex from 'vuex';
import Vue from 'vue';

window.routes = [
    { path: '/', component: Users },
    { path: '/requests', component: Requests }
];

window.store = new Vuex.Store({
    state: {
        register_url,
        feature_url,
        request_url,
        users,
        requests,
    },
    getters : {
        registrationUrl: state => {
            return state.register_url;
        },
        featureUrl: state => {
            return state.feature_url;
        },
        requestUrl: state => {
            return state.request_url;
        },
        users: state => {
            return state.users;
        },
        requests: state => {
            return state.requests;
        },
        /**
         * Filtering requests
         */
        filteredRequests: state => keyword => {
            return state.requests.filter(request => {
                return request.user.fullname.toLowerCase().includes(keyword.toLowerCase())
                    || (request.office && request.office.address.toLowerCase().includes(keyword.toLowerCase()))
                    || ( ! request.office && 'at home'.includes(keyword.toLowerCase()))
                    ||  request.date.includes(keyword.toLowerCase())
                    ||  request.status.includes(keyword.toLowerCase())
            })
        }
    },
    mutations: {
        /**
         * Adds new registered user to list
         */
        register (state, user) {
            state.users.push(user);
        },
        /**
         * Renew request status after approve or cancel
         */
        request_updated (state, payload) {
            $.each(state.requests, ( index, value ) => {
                if(state.requests[index].id == payload['request_id'])
                {
                    let tmp = state.requests[index];

                    tmp.status = payload['status']; 
                    
                    Vue.set(state.requests, index, tmp);
                }
            });
            
        },
    }
});