/**
 * Employe vue store
 */

import Employee from './pages/employee/EmployeeComponent.vue';
import Vuex from 'vuex';

window.routes = [
    { path: '/', component: Employee },
];

window.store = new Vuex.Store({
    state: {
        features,
        offices,
        notifications,
        request_url,
        notification_url,
        requests,
        request_update_url
    },
    getters : {
        features: state => {
            return state.features;
        },
        notifications: state => {
            return state.notifications;
        },
        offices: state => {
            return state.offices;
        },
        requestUrl: state => {
            return state.request_url;
        },
        requestUpdateUrl: state => {
            return state.request_update_url;
        },
        notificationUrl: state => {
            return state.notification_url;
        },
        requests: state => {
            return state.requests;
        },
    },
    mutations: {
        /**
         * Removes canceled request from list
         */
        request_updated (state, request_id) {
            state.requests = state.requests.filter(function(item) { 
                return item.id !== request_id
            })            
        },
        /**
         * Adds new request to list
         */
        request_created (state, request) {
            state.requests.push(request);
        },
    }
});