## Starting app

- php artisan key:generate - creates app key
- npm install - installs node modules
- php artisan migrate - creates database table
- php artisan db:seed - creates demo data (creates ADMIN: **admin@admin.com pass: admin** and EMPLOYEE **user@user.com pass: user**)

You can test app by running *phpunit* command.

## Code overview

App framework is Laravel. For fronted mainly is used Vue.js.

### Folders


- Docs\database.mwb - Contains Mysql workbench file

- App\Routes\web.php - App routes


- App\Http\Controllers\AuthController.php - Controls login and logout
- App\Http\Controllers\FeaturesController.php - Controls users feature updating 
- App\Http\Controllers\HomeController.php - Controls way to the front depending by user level
- App\Http\Controllers\RegisterController.php - Controls user creation logic
- App\Http\Controllers\RequestsController.php - Controls creation and updates of user work-at-home requests


- App\Http\Middlaware\Admin.php - Middllaware used for some routes like new user registration


- App\Models - Contains all needed models


- App\Database\Migrations - Creates all needed tables and their relations
- App\Database\Seeds - Creates demo data

- App\Resources\Assets\Sass - Sass files 
- App\Resources\Assets\Vue - VUE files inside separated by namespaces depinding on who uses them(admin, employee)

- App\Resources\Views - Blade files


## Tasks extras

1. Multiple office locations, and request can be done to choose any one of the offices or home - **DONE**
2. PHP unit testing -  **DONE**
3. Making use of jQuery and AJAX - **DONE: VUE JS ADDED TO**
4. Implement your own idea which you think would be useful for this application **DIDINT. ITS VERY BASIC APP SO THERE IS A LOT OF IDEAS WHAT CAN BE IMPLEMENT, BUT BECOUSE DOING THIS TAX AFTER WORK I HAD SHORT TIME**
