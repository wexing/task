<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'Home@hello');

Route::view('/login', 'pages.login')->middleware('guest')->name('login_page');

Route::post('/login', 'AuthController@login')->name('login');


Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/', 'HomeController@home')->name('home');
    Route::get('/logout', 'AuthController@logout')->name('logout');

    Route::group(['middleware' => ['admin']], function () {
        Route::post('/register', 'RegisterController@register')->name('register');
        Route::post('/features/update', 'FeaturesController@update')->name('features_update');
    });

    Route::post('/request/update', 'RequestsController@update')->name('request_update');
    Route::post('/request/create', 'RequestsController@create')->name('request_create');
    Route::post('/notification/seen', 'NotificationsController@seen')->name('notification_seen');
});