<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRequest extends Model
{
        protected $table = 'users_requests';
        protected $with = ['user', 'office'];
        
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'is_sick', 'offices_id', 'date', 'hours', 'status',
        ];

        public function user()
        {
                return $this->belongsTo('App\Models\User', 'users_id');
        }

        public function office()
        {
                return $this->belongsTo('App\Models\office', 'offices_id');
        }

}
