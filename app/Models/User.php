<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'fullname', 'email', 'password', 'level',
        ];

        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password', 'remember_token',
        ];

        public function features()
        {
                return $this->hasMany('App\Models\UserFeature', 'users_id');
        }

        public function requests()
        {
                return $this->hasMany('App\Models\UserRequest', 'users_id');
        }

        public function notifications()
        {
                return $this->hasMany('App\Models\UserNotification', 'users_id');
        }
}
