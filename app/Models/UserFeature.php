<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFeature extends Model
{
        protected $table = 'users_features';
        protected $with = ['feature'];
        
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'users_id', 'features_id', 'active',
        ];

        public function feature()
        {
                return $this->hasOne('App\Models\Feature', 'id', 'features_id');
        }
}
