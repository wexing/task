<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class NotificationsController extends Controller
{
        /**
         * Marks user notification as seen
         *
         * @param Request $request
         * @return void
         */
        public function seen(Request $request)
        {
                Auth::user()
                    ->notifications()
                    ->find($request->input('notification_id'))
                    ->fill(['seen' => 1])
                    ->save();
        }
}
