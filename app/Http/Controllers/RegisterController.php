<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Feature;
use App\Models\UserFeature;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{   
        /**
         * Creates new user
         *
         * @param Request $request
         * @return json Errors or registered user data
         */
        public function register(Request $request)
        {
                $validator = \Validator::make($request->all(), [
                    'fullname' => 'required|max:70',
                    'email' => 'required|email',
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->all()]);
                }

                //checking does user email is unique
                if(User::where('email', $request->input('email'))->first())
                {
                    return response()->json(['errors'=> ['User already exists']]);
                }

                $password = str_random(8);

                if ($user = User::create([
                    'fullname' => $request->input('fullname'),
                    'email' => $request->input('email'),
                    'level' => 'employee',
                    'password' => Hash::make($password),
                ]))
                {
                    $this->create_user_features($user);

                    $data = $user->toArray();
                    
                    Mail::send('emails.account_created', $data + ['password' => $password], function($message) use ($data)
                    {
                        $message->from('no-reply@site.com', "Site name");
                        $message->subject("Your account was created");
                        $message->to($data['email']);
                    });

                    return json_encode($data + ['features' => $user->features()->get()->toArray()]);
                }
                else
                {
                    return response()->json(['errors'=> ['System error']]);
                }
        }

        /**
         * Creates default user features
         *
         * @param User $user
         * @return void
         */
        private function create_user_features(User $user)
        {
                foreach(Feature::all() AS $feature)
                {
                    $user->features()
                        ->create([
                        'users_id' => $user->id,
                        'features_id' => $feature->id,
                        'active' => false,
                    ]);
                }
        }
}
