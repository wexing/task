<?php

namespace App\Http\Controllers;

use App\Models\UserFeature;
use App\Models\User;
use Illuminate\Http\Request;

class FeaturesController extends Controller
{       
        /**
         * Changes state of user feature
         *
         * @param Request $request
         * @return void
         */
        public function update(Request $request)
        {
                foreach($request->input('features') AS $feature)
                {
					User::find($request->input('user_id'))
						->features()
						->find($feature['id'])
						->fill(['active' => $feature['active']])
						->save();
                }
        }
}
