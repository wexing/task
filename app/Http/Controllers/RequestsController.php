<?php

namespace App\Http\Controllers;

use App\Models\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;

class RequestsController extends Controller
{
        /**
         * Administrator approves or cancels employee request
         *
         * @param Request $request
         * @return void
         */
        public function update(Request $request)
        {   
                User::find($request->input('user_id'))
                    ->requests()
                    ->find($request->input('request_id'))
                    ->fill(['status' => $request->input('status')])
                    ->save();

                if($request->input('user_id') != Auth::user()->id)
                {
                    User::find($request->input('user_id'))
                        ->notifications()
                        ->create([
                            "info" => "Status has been changed of your request",
                            "seen" => 0,
                        ]);
                }
        }

        /**
         * Creates new home work request
         *
         * @param Request $request
         * @return json errors or request data
         */
        public function create(Request $request)
        {
                $validator = \Validator::make($request->all(), [
                    'date' => 'required|date_format:"Y-m-d"',
                    'office' => 'required',
                ]);

                if ($validator->fails())
                {
                    return response()->json(['errors'=>$validator->errors()->all()]);
                }

                $date = $request->input('date');
                $tomorrow = date('Y-m-d', strtotime("+1 day"));

                //A request has to be made at least 8hours before the end of the previous day
                if( ! $request->input('is_sick') AND ($date < $tomorrow OR ($date == $tomorrow AND date('H') >= 20)))
                {
                    return response()->json(['errors'=> ['A request has to be made at least 8hours before the end of the previous day']]); 
                }

                $today = date('Y-m-d');

                //A can be made by 8am of that same work day
                if( $request->input('is_sick') AND $date == $today AND date('H') >= 8)
                {
                    return response()->json(['errors'=> ['A can be made by 8am of that same work day']]); 
                }

                $office = $request->input('office');

                if($request = Auth::user()
                    ->requests()
                    ->create([
                        "offices_id" => ($office != 'home' ? $office : null),
                        "hours" => $request->input('hours') ?: null,
                        "date" => $date,
                        "is_sick" => $request->input('is_sick') ? 1 : 0,
                        "status" => "pending"
                    ]))
                {
                    return $request->tojson();
                }
                else
                {
                    return response()->json(['errors'=> ['System error']]);
                }
        }
}
