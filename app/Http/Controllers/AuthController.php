<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class AuthController extends Controller
{
        /**
         * Logins user
         *
         * @param Request $request
         * @return redirect back if error or to home if success
         */
        public function login(Request $request)
        {
                $this->validate($request, [
                    'email' => 'required|email',
                    'password' => 'required|alphaNum|min:4',
                ]);

                $credentials = $request->only('email', 'password');

                if (Auth::attempt($credentials, $request->input('remember'))) 
                {
                    return redirect()->route('home');
                }
                else
                {
                    return back()->withErrors('Wrong credentials');
                }
        }

        /**
         * Logouts user
         *
         * @param Request $request
         * @return redirect home
         */
        public function logout(Request $request)
        {
                Auth::logout();

                return redirect()->route('login_page');
        }
}
