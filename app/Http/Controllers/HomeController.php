<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\User;
use App\Models\UserRequest;
use App\Models\Office;

class HomeController extends Controller
{
        /**
         * Routes user with needed data to view according his level
         *
         * @return \Illuminate\View\View
         */
        public function home()
        {
                switch(Auth::user()->level)
                {
                    case 'admin':
                        $users = User::where('level', 'employee')->with('features')->get()->toJson();

                        return view('pages.admin')->with([
                            'users' => $users,
                            'requests' => UserRequest::where('date', '>=' , date('Y-m-d'))->get()->toJson()
                        ]);
                    break;
                    case 'employee':
                        return view('pages.employee')->with([
                            'features' => Auth::user()->features()->get()->toJson(),
                            'offices' => Office::get()->toJson(),
                            'notifications' => Auth::user()->notifications()->where('seen', 0)->get()->toJson(),
                            'requests' => Auth::user()->requests()->where('date', '>=' , date('Y-m-d'))->where('status', '!=' , 'cancelled')->get()->toJson()
                        ]);
                    break;
                    default:
                        throw new \Exception('Error');
                }
        }
}
