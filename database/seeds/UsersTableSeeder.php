<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            DB::table('users')->insert([[
                    'fullname' => str_random(10) . " " . str_random(10),
                    'email' => 'admin@admin.com',
                    'password' => Hash::make('admin'),
                    'level' => 'admin',
                ],
                [
                    'fullname' => str_random(10) . " " . str_random(10),
                    'email' => 'user@user.com',
                    'password' => Hash::make('user'),
                    'level' => 'employee',
                ]
            ]);
    }
}
