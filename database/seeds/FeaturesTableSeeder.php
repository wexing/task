<?php

use Illuminate\Database\Seeder;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('features')->insert([
            ['name' => 'email access granted'],
            ['name' => 'git repository granted'],
            ['name' => 'microsoft office licence'],
            ['name' => 'trello access granted'],
        ]);

        DB::table('users_features')->insert([
            ['features_id' => 1, 'users_id' => 2, 'active' => 1],
            ['features_id' => 2, 'users_id' => 2, 'active' => 0],
            ['features_id' => 3, 'users_id' => 2, 'active' => 1],
            ['features_id' => 4, 'users_id' => 2, 'active' => 0],
        ]);
    }
}
