<?php

use Illuminate\Database\Seeder;

class OfficesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('offices')->insert([
            ['address' => 'Office 1'],
            ['address' => 'Office 2'],
            ['address' => 'Office 3'],
            ['address' => 'Office 4'],
        ]);
    }
}