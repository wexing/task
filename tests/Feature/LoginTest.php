<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test */
    public function testAdminLoginAndLogoutWorking()
    {
            $response = $this->json('POST', '/login', ['email' => 'admin@admin.com', 'password' => 'admin', '_token' => csrf_token()]);
            $this->assertAuthenticated($guard = null);

            $this->json('GET', '/logout', []);
            $this->assertGuest($guard = null);
    }

    /** @test */
    public function testEmployeeLoginAndLogoutWorking()
    {
            $response = $this->json('POST', '/login', ['email' => 'user@user.com', 'password' => 'user', '_token' => csrf_token()]);
            $this->assertAuthenticated($guard = null);

            $this->json('GET', '/logout', []);
            $this->assertGuest($guard = null);
    }

    /** @test */
    public function testCantLoginWithWrong()
    {
            $response = $this->json('POST', '/login', ['email' => 'addmin@admin.com', 'password' => 'adminsss', '_token' => csrf_token()]);
            $this->assertGuest($guard = null);
    }
}
