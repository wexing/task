<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test */
    public function testRegistrationIsWorking()
    {
            $response = $this->json('POST', '/login', ['email' => 'admin@admin.com', 'password' => 'admin', '_token' => csrf_token()]);
            $this->assertAuthenticated($guard = null);

            $email = uniqid() . '@admin.com';
            $response = $this->json('POST', '/register', ['email' => $email, 'fullname' => 'fullname', '_token' => csrf_token()]);

            $decoded = json_decode($response->original, true);
            $user_id = $decoded['id'];

            $response
                ->assertStatus(200)
                ->assertJson([
                    'level' => 'employee',
                ]);

            $this->assertDatabaseHas('users', [
                'email' => $email
            ]);

            $this->assertDatabaseHas('users_features', [
                'users_id' => $user_id
            ]);
    }

    /** @test */
    public function testOnlyAdminCanRegisterUser()
    {
            $this->json('POST', '/login', ['email' => 'user@user.com', 'password' => 'user', '_token' => csrf_token()]);
            $this->assertAuthenticated($guard = null);

            $email = uniqid() . '@admin.com';
            $response = $this->json('POST', '/register', ['email' => $email, 'fullname' => 'fullname', '_token' => csrf_token()]);

            $response->assertStatus(500);
    }
}
