<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Auth;

class RequestTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

   /* /** @test */
    public function testEmployeeCanMakeRequest()
    {
            $response = $this->json('POST', '/login', ['email' => 'user@user.com', 'password' => 'user', '_token' => csrf_token()]);
            $this->assertAuthenticated($guard = null);

            $response = $this->json('POST', '/request/create', ['date' => date('Y-m-d', strtotime("+1 day")), 'office' => 1, '_token' => csrf_token()]);
            
            $response->assertStatus(200)
                ->assertJson([
                    'created_at' => true,
            ]);
    }

    /* /** @test */
     public function testAdminCanApproveRequest()
     {
            $response = $this->json('POST', '/login', ['email' => 'user@user.com', 'password' => 'user', '_token' => csrf_token()]);
            $this->assertAuthenticated($guard = null);

            $decoded_user = json_decode($response->original, true);
            $user_id = Auth::user()->id;
 
            $response = $this->json('POST', '/request/create', ['date' => date('Y-m-d', strtotime("+1 day")), 'office' => 1, '_token' => csrf_token()]);

            $response->assertStatus(200)
                 ->assertJson([
                    'created_at' => true,
            ]);

            $decoded_request = json_decode($response->original, true);

            $response = $this->json('POST', '/login', ['email' => 'admin@admin.com', 'password' => 'admin', '_token' => csrf_token()]);

            $this->assertAuthenticated($guard = null);
       
            $response = $this->json('POST', '/request/update', [
                'user_id' => $user_id, 
                'request_id' => $decoded_request['id'], 
                'status' => 'approved', 
                '_token' => csrf_token()
            ]);
            
            $response->assertStatus(200);

            $this->assertDatabaseHas('users_requests', [
                'users_id' => $user_id,
                'status' => 'approved'
            ]);
     }  
}
